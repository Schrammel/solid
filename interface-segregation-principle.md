## Interface Segregation Principle (ISP) (Princípio da Segregação de Interface)

O Princípio da Segregação de Interface trata da coesão de interfaces e diz que clientes não devem ser forçados a depender de métodos que não usam.

Exemplo de pseudocódigo ruim:
```java

public interface Funcionario {
    String getFuncao();
    String getComissao();
}

public class Vendedor implements Funcionario {
    private final String nome;
    private final String FUNCAO = "Vender";
    private final List< Venda > vendas = new ArrayList<>();
    private final CalculoComissao calculoComissao = new CalculoComissao();

    public Vendedor( String nome ) {
        // ...
    }

    public String getFuncao() {
        return this.FUNCAO;
    }

    public float getComissao() {
        return this.calculoComissao( this );
    }
}

public class Recepcionista implements Funcionario {
    private final String nome;
    private final String FUNCAO = "Recepcionar";

    public Recepcionista( String nome ) {
        // ...
    }
    public String getFuncao() {
        return this.FUNCAO;
    }

    public float getComissao() {
        return 0f;
    }
}

```
O método getComissao não e relevante para um Funcionario da Recepcao, logo a interface Funcionario não deveria ter o método.

Exemplo de pseudocódigo bom:

```java
public interface Funcionario {
    String getFuncao();
}

public class Vendedor implements Funcionario {
    private final String nome;
    private final String FUNCAO = "Vender";
    private final List< Venda > vendas = new ArrayList<>();
    private final CalculoComissao calculoComissao = new CalculoComissao();

    public Vendedor( String nome ) {
        // ...
    }

    public String getFuncao() {
        return this.FUNCAO;
    }

    public getComissao() {
        return this.calculoComissao( this );
    }
}

public class Recepcionista implements Funcionario {
    private final String nome;
    private final String FUNCAO = "Recepcionar";

    public Recepcionista( String nome ) {
        // ...
    }

    public String getFuncao() {
        return this.FUNCAO;
    }
}

```