## Liskov Substitution Principle ( Princípio da Substituição de Liskov )  


> Se um gato possui raça e patas, e um cachorro possui raça, patas e tipoDoPelo, logo Cachorro extends Gato? Pode parecer engraçado, mas é o mesmo caso que os anteriores: herança por preguiça, por comodismo, por que vai dar uma ajudinha. A relação “é um” não se encaixa aqui, e vai nos gerar problemas.

A moral é que uma especializacao de uma classe não pode alterar o comportamento da classe base.
Em resumo, se eu substituir uma subclasse pela classe base, o comportamento externo tem que ser o mesmo!.

Exemplo de pseudocódigo ruim:

```java
public class Funcionario {
    private final String nome;
    private final List< Venda > vendas = new ArrayList<>();

    public Funcionario( String nome ) {
        this.nome = "joao";
    }

    public float getValorVendas() {
        float total = 0f;
        for( Venda venda : this.vendas ) {
            total += venda.getValor();
        }
        return total;
    }
}

public class Recepcionista extends Funcionario {
    public FuncionarioDefault( String nome ) {
        super( nome );
    }
    
    public float getValorVendas() {
        throw new Erro( "Recepcionista não tem vendas" );
    }
}
```
Uma recepcionista é um funcionario? SIM
Mas essa herança está correta? NÃO

Ao alterar o uso de Funcionario para Recepcionista terá alteração no comportamento
sendo assim, isso está proibido.


Exemplo de pseudocódigo bom:

```java
public interface Pessoa implements Individuo {
    private final String nome;

    public Pessoa( String nome ) {
        this.nome = nome;
    }
}

public interface Cargo {
    public void trabalhar();
}

public interface Vendedor extends Cargo {
     public float getValorVendas() {
         return 0f;
     }
}

public interface Recepcionista extends Cargo {

}

public class Trabalhador extends Pessoa implements Individuo, Cargo {
    private final String nome;

    public Pessoa( Individuo ser, Cargo cargo ) {
        this.individuo = ser;
        this.cargo = cargo;
    }
}
```

Um funcionario sempre vai ter o comportamento de uma Pessoa.