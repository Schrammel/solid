## Single Responsibility ( Responsabilidade Unica )  

Uncle Bob diz eu seu livro clean code, que se uma classe tem mais de um motivo para ser alterada ela já está ferindo o princípio da responsabilidade única.

Se ao se referir a uma determinada classe, você diz por exemplo: 
> “minha classe tem as informações do cliente e salva o mesmo no banco de dados”

Perceba que o “e” na frase, indica mais de uma responsabilidade, ferindo assim o SRP( single responsability principle ).

Exemplo de pseudocódigo ruim:
```java

public class Funcionario {
    private final String nome;
    private final List< Venda > vendas = new ArrayList<>();

    public Funcionario( String nome ) {
        this.nome = salarioBruto;
    }

    public float calculaComissao() {
        float comissao = 0f;
        return this.getValorVendas() * comissao;
    }

    public float getValorVendas() {
        float total = 0f;
        for( Venda venda : this.vendas ) {
            total += venda.getValor();
        }
        return total;
    }
}
```
A classe funcionário deve representar o funcionário, e nada alem disso.


Exemplo de pseudocódigo bom:

```java
public class Funcionario {
    private final String nome;
    private final List< Venda > vendas = new ArrayList<>();

    public Funcionario( String nome ) {
        // ...
    }
    
    public float getValorVendas() {
        float total = 0f;
        for( Venda venda : this.vendas ) {
            total += venda.getValor();
        }
        return total;
    }
}

public class CalculoComissao {
    private final float comissao = 0.1;

    public float calculaCalculoComissao( Funcionario funcionario ) {
        return funcionario.getValorVendas() * this.comissao
    }
}

Funcionario joao = new Functionario( "Joao" );
Funcionario maria = new Functionario( "Maria" );

CalculoComissao calculo = new CalculoComissao();
calculo.calculaComissao( joao ); 
calculo.calculaComissao( maria ); 

```