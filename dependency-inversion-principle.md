## Dependency Inversion Principle (DIP) (Princípio da Inversão de Dependências)

De forma geral, não devemos depender de implementações  e sim de abstrações.

Exemplo de pseudocódigo ruim:
```java

public class Funcionario {
    private final String nome;
    private final List< Venda > vendas = new ArrayList<>();
    private final CalculoComissao calculoComissao = new CalculoComissao();

    public Funcionario( String nome ) {
        // ...
    }
    public getComissao() {
        return this.calculoComissao( this );
    }
}

public class CalculoComissao {
    private final float comissao = 0.1;

    public float calculaCalculoComissao( Funcionario funcionario ) {
        float vendas = 0f;
        vendas+= funcionario.getValorVendas();
        return vendas * this.comissao
    }
}


```
O field CalculoComissao não pode ser facilmente trocado por outro tipo, caso tenha a necessidade de
trocar o comportamento do calculo teriamos que quebrar a API.


Exemplo de pseudocódigo bom:

```java
public class Funcionario {
    private final String nome;
    private final List< Venda > vendas = new ArrayList<>();
    private final CalculoComissao calculoComissao = new ComissaoNormal();

    public Funcionario( String nome ) {
        // ...
    }
    public float getComissao() {
        return this.calculoComissao( this );
    }
}

public interface CalculoComissao {
    float calculaCalculoComissao( Funcionario funcionario );
}

public class ComissaoNormal implements CalculoComissao {
    private final float comissao = 0.1f;
    
    public float calculaCalculoComissao( Funcionario funcionario ) {
        float vendas = 0f;
        vendas+= funcionario.getValorVendas();
        return vendas * this.comissao;
    }
}

public class ComissaoAvancada implements CalculoComissao {
    private final float comissao = 0.2f;

    public float calculaCalculoComissao( Funcionario funcionario ) {
        float vendas = -1000f;
        vendas+= funcionario.getValorVendas();
    
        if ( vendas <  0 ) {
            vendas = 0
        }

        return vendas * this.comissao;
    }
}

```