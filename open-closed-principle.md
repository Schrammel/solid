## Open Closed Principle ( Principio do Aberto/Fechado )  

“Entidades de software (classes, módulos, funções, etc.) devem ser abertas para extensão mas fechadas para modificação.”

A moral da história é a seguinte: quando eu precisar estender o comportamento de um código, eu crio código novo ao invés de alterar o código existente.

Considere esse exemplo:
```java
public class CalculoComissao {
    private final float COMISSAO = 0.1f;

    public float calculaCalculoComissao( Funcionario funcionario ) {
        return funcionario.getValorVendas(); * this.COMISSAO;
    }
}
```
Surge uma demanda, caso o funcionario seja do tipo 1, então o calculo de comissão deve ser diferente, logo temos em mente a seguinte alteração:

```java
public class CalculoComissao {
    private final float COMISSAO = 0.1f;
    private final float COMISSAO2 = 0.2f;

    public float calculaCalculoComissao( Funcionario funcionario ) {
        float vendas = 0f;
        vendas+= funcionario.getValorVendas();

        if ( funcionario.getCategoria.equals( 1 ) ) {
            return vendas * this.COMISSAO;
        } else {
            vendas = -1000f;
            if ( vendas <  0 ) {
                vendas = 0
            }
            return vendas * this.COMISSAO2;
        } 
    }
}
```
O exemplo acima viola a regra do Aberto para Modificacao, mas então como devemos fazer?

Devemos criar uma nova classe para o comportamento adequado, veja o exemplo.
```java

public interface CalculoComissao {
    float calculaCalculoComissao( Funcionario funcionario );
}

public class ComissaoNormal implements CalculoComissao {
    private final float COMISSAO = 0.1f;
    
    public float calculaCalculoComissao( Funcionario funcionario ) {
        return funcionario.getValorVendas() * this.COMISSAO;
    }
}

public class ComissaoAvancada implements CalculoComissao {
    private final float COMISSAO = 0.2f;

    public float calculaCalculoComissao( Funcionario funcionario ) {
        float vendas = -1000f;
        vendas+= funcionario.getValorVendas();
        if ( vendas <  0 ) {
            vendas = 0
        }
        return vendas * this.COMISSAO;
    }
}
```